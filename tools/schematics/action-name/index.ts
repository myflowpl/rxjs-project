import {
  Rule,
  SchematicContext,
  SchematicsException,
  Tree,
  chain,
  externalSchematic,
  apply,
  applyTemplates,
  url,
  noop,
  filter,
  template,
  move,
  mergeWith,
} from '@angular-devkit/schematics';
import * as ts from 'typescript';
import {
  stringUtils,
  buildRelativePath,
  insertImport,
  NoopChange,
  ReplaceChange,
  InsertChange,
  getProjectPath,
  omit,
  parseName,
} from '@ngrx/schematics/schematics-core';
import { Schema as ContainerOptions } from './schema';
import { join } from 'path';

function addStateToComponent(options: ContainerOptions) {
  return (host: Tree) => {

    // const componentPath = options.path;
    const componentPath = join('apps', options.project, 'src', 'app', options.path);
    // `/${options.path}/` +
    // (options.flat ? '' : stringUtils.dasherize(options.name) + '/') +
    // stringUtils.dasherize(options.name) +
    // '.component.ts';

    const text = host.read(componentPath);

    if (text === null) {
      throw new SchematicsException(`File ${componentPath} does not exist.`);
    }

    const sourceText = text.toString('utf-8');

    // console.log('cpath', componentPath, sourceText);

    const source = ts.createSourceFile(
      componentPath,
      sourceText,
      ts.ScriptTarget.Latest,
      true
    );
    // console.log('source', source);
    // ts.updateSourceFileNode();
    console.log('NODE', ts.createEnumMember(
      ts.createIdentifier('New'),
      ts.createStringLiteral('[Counter] New')
    ).getText())

    source.forEachChild(node => console.log('KIND', ts.SyntaxKind[node.kind]));
    source.forEachChild(node => {
      if (ts.isEnumDeclaration(node)) {
        console.log('Node members', node.members[node.members.length - 1]);
        // console.log('Node', node.getText());
        // const node2 = ts.updateEnumDeclaration(node, [], [], ts.createIdentifier('myEnum'), [
        //   ts.createEnumMember(
        //     ts.createIdentifier('New'),
        //     ts.createStringLiteral('[Counter] New')
        //   )
        // ])
        // console.log('Node1', node2);

        // console.log('Node1', node.getText());
      }
    });
    // console.log('node', source.getChildAt(0))

    // const stateImportPath = buildRelativePath(componentPath, statePath);
    // const storeImport = insertImport(
    //   source,
    //   componentPath,
    //   'Store',
    //   '@ngrx/store'
    // );
    // const stateImport = options.state
    //   ? insertImport(
    //     source,
    //     componentPath,
    //     `* as fromStore`,
    //     stateImportPath,
    //     true
    //   )
    //   : new NoopChange();

    // const componentClass = source.statements.find(
    //   stm => stm.kind === ts.SyntaxKind.ClassDeclaration
    // );
    // const component = componentClass as ts.ClassDeclaration;
    // const componentConstructor = component.members.find(
    //   member => member.kind === ts.SyntaxKind.Constructor
    // );
    // const cmpCtr = componentConstructor as ts.ConstructorDeclaration;
    // const { pos } = cmpCtr;
    // const stateType = options.state
    //   ? `fromStore.${options.stateInterface}`
    //   : 'any';
    // const constructorText = cmpCtr.getText();
    // const [start, end] = constructorText.split('()');
    // const storeText = `private store: Store<${stateType}>`;
    // const storeConstructor = [start, `(${storeText})`, end].join('');
    // const constructorUpdate = new ReplaceChange(
    //   componentPath,
    //   pos,
    //   `  ${constructorText}\n\n`,
    //   `\n\n  ${storeConstructor}`
    // );

    // const changes = [storeImport, stateImport, constructorUpdate];
    // const recorder = host.beginUpdate(componentPath);

    // for (const change of changes) {
    //   if (change instanceof InsertChange) {
    //     recorder.insertLeft(change.pos, change.toAdd);
    //   } else if (change instanceof ReplaceChange) {
    //     recorder.remove(pos, change.oldText.length);
    //     recorder.insertLeft(change.order, change.newText);
    //   }
    // }

    // host.commitUpdate(recorder);

    // return host;
  };
}

export default function (options: ContainerOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    console.log('options', options)
    // options.path = getProjectPath(host, options);

    // const parsedPath = parseName(options.path, options.names);
    // options.names = parsedPath.name;
    // options.path = parsedPath.path;


    return chain([
      addStateToComponent(options),
    ])(host, context);
  };
}

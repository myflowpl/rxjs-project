export interface Schema {
  /**
   * The path to create the component.
   */
  path?: string;
  /**
   * The name of the project.
   */
  project?: string;
  /**
   * The name of the component.
   */
  names: string;
}

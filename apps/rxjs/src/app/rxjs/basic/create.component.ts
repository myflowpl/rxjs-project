import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  Observable,
  fromEvent,
  interval,
  of,
  empty,
  EMPTY,
  range,
  timer,
  throwError
} from 'rxjs';
import { share } from 'rxjs/operators';
import { ListComponent } from '../../shared/list/list.component';

@Component({
  selector: 'app-create',
  template: `
    <h1>Create Observables</h1>
    <p>
      dostępne metody: Create, Defer, Empty/Never/Throw, From, Interval, Just,
      Range, Repeat, Start, and Timer
    </p>
    <button #btn class="btn btn-primary">Button</button>
  `,
  styles: []
})
export class CreateComponent implements OnInit {
  @ViewChild('btn')
  btn: ElementRef;
  constructor(private list: ListComponent) { }

  ngOnInit() {
    const log = (...args) => this.list.add(...args);
    const button = this.btn.nativeElement;

    const stream$ = interval(500);
    // const stream$ = of({id: 2, name: 'Piotr'});
    // const stream$ = empty();
    // const stream$ = EMPTY;
    // const stream$ = range(2, 4);
    // const stream$ = timer(2000);
    // const stream$ = throwError('custom error');

    const sub = stream$.subscribe(
      val => log('next', val),
      err => log('error', err),
      () => log('complete')
    );

  }
}

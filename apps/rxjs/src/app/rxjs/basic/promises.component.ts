import {
  Component,
  OnInit,
  ViewChild,
  ViewRef,
  ElementRef
} from '@angular/core';
import { fromEvent, Observable, Observer, Subscription } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { ListComponent } from '../../shared/list/list.component';

@Component({
  selector: 'app-promises',
  template: `
    <h1>Promise</h1>
    <button #btn class="btn btn-primary">Button</button>
  `,
  styles: []
})
export class PromisesComponent implements OnInit {
  @ViewChild('btn')
  btn: ElementRef;

  constructor(private list: ListComponent) { }

  async ngOnInit() {
    const log = (...args) => this.list.add(...args);
    const button = this.btn.nativeElement;

    /**
     * tworzenie promisa
     */
    const myTask = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ title: 'my data after: 2s' });
      }, 2000);

    });

    myTask.then(data => log(data));

    // cache
    // setTimeout(() => {
    //   myTask.then(data => log(data));
    // }, 3000);

    /**
     * Chaining / łączenie operacji
     */
    // fetch('/api/long')
    //   .then(req => req.json())
    //   .then(data => fetch('/api/parse', { body: data }))
    //   .then(data2 => log('DATA 2', data2))
    //   .catch(err => log('ERR', err));

    /**
     * async await
     */
    // try {

    //   const req = await fetch('/api/long');                    // ajax 1

    //   const data = await req.json();                           // rozpakowanie danych

    //   const data2 = await fetch('/api/parse', { body: data }); // ajax 2

    //   log('DATA 2', data2);
    // } catch (error) {
    //   log('ERR', error);
    // }
  }
}

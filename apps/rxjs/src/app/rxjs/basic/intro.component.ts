import {
  Component,
  OnInit,
  ViewChild,
  ViewRef,
  ElementRef,
  Inject,
  OnDestroy
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { ListComponent } from '../../shared/list/list.component';

@Component({
  selector: 'app-intro',
  template: `
    <h1>RxJS Playground</h1>
    <button #btn class="btn btn-primary">Button</button>
  `,
  styles: []
})
export class BasicIntroComponent implements OnInit, OnDestroy {
  @ViewChild('btn')
  btn: ElementRef;

  constructor(private list: ListComponent) { }

  ngOnInit() {
    const button = this.btn.nativeElement;
    const log = (...args) => this.list.add(...args);

    // WORK

    button.addEventListener('click', log);
  }
  ngOnDestroy(): void {

  }
}

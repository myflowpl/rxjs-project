import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  Observable,
  fromEvent,
  combineLatest,
  BehaviorSubject,
  interval,
  of,
  EMPTY
} from 'rxjs';
import {
  startWith,
  map,
  share,
  switchMap,
  catchError,
  takeUntil
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { ListComponent } from '../../shared/list/list.component';

@Component({
  selector: 'app-operators',
  template: `
    <h1>Operatory</h1>
    <p>
      zaawansowane: switchMap, debounceTime throttleTime combineLatest retry
      merge delay bufferTime switchMap takeUntil
    </p>
    <input
      #input
      type="text"
      id="textInput"
      class="form-control"
      placeholder="Enter Query..."
      autocomplete="false"
    />
    <pre>{{ text }}</pre>
    <button #btn class="btn btn-primary">Button</button>

    <pre>{{ data$ | async | json }}</pre>
  `,
  styles: []
})
export class OperatorsComponent implements OnInit {
  @ViewChild('input')
  input: ElementRef;
  @ViewChild('btn')
  btn: ElementRef;
  text: string;

  data$: Observable<any>;
  constructor(private list: ListComponent) { }
  ngOnInit() {
    const log = (...args) => this.list.add(...args);
    const button = this.btn.nativeElement;
    const input = this.input.nativeElement;

    const btn$: Observable<MouseEvent> = fromEvent(button, 'click');
    const input$: Observable<MouseEvent> = fromEvent(input, 'keyup');

    const interval$ = interval(1000);

    this.data$ = ajax('/api/long').pipe(
      map(res => res.response),
      startWith({ id: 2, name: 'guest' }),
      takeUntil(btn$)
    );

    // this.data$.subscribe(
    //   data => log('DATA', data),
    //   err => log('ERR', err)
    // );

  }
}

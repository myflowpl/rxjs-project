import {
  Component,
  OnInit,
  ViewChild,
  ViewRef,
  ElementRef
} from '@angular/core';

import { fromEvent, Observable, Observer, Subscription, EMPTY } from 'rxjs';
import { share } from 'rxjs/operators';
import { ListComponent } from '../../shared/list/list.component';

@Component({
  selector: 'app-observable',
  template: `
    <h1>Observable</h1>
    <button #btn class="btn btn-primary">Button</button>
  `,
  styles: []
})
export class ObservableComponent implements OnInit {
  @ViewChild('btn')
  btn: ElementRef;
  constructor(private list: ListComponent) { }

  ngOnInit() {
    const log = (...args) => this.list.add(...args);
    const button = this.btn.nativeElement;

    // ---------------------------------------------------------------------

    // Źródło
    const btn$: Observable<MouseEvent> = fromEvent(button, 'click');

    // Subskrypcja
    log('SUB')
    const sub: Subscription = btn$.subscribe(
      val => log('next', val),
      err => log('error', err),
      () => log('complete')
    );
    // unsubscribe
    sub.unsubscribe();

    // ---------------------------------------------------------------------

    /**
     * Tworzenie własnego źródła
     */
    function myFromEvent(el, eventName): Observable<any> {
      return EMPTY; // TODO napisać implementację
    }
    const btn2$ = myFromEvent(button, 'click');

    // Subskrybent A
    // log('SUB A')'
    // const subA = btn2$.subscribe(
    //   val => log('A next', val),
    //   err => log('A error', err),
    //   () => log('A complete')
    // );
    // setTimeout(() => {
    //   log('UN SUB A');
    //   subA.unsubscribe();
    // }, 2000);

    // Subskrybent B
    // log('SUB B');
    // const subB = btn2$.subscribe(
    //   val => log('B next', val),
    //   err => log('B error', err),
    //   () => log('B complete')
    // );
    // setTimeout(() => {
    //   log('UN SUB B');
    //   subB.unsubscribe();
    // }, 2000);



    // ---------------------------------------------------------------------

    const apiUrl = '/api/wikipedia?q=apokalipsa';
    /**
     * Promise based request
     */
    // const ctrl = new AbortController();
    // fetch(apiUrl, { signal: ctrl.signal })
    //   .then(res => res.json())
    //   .then(data => log('SUCCESS', data))
    //   .catch(err => log('ERROR', err));

    // setTimeout(() => {
    //   // ctrl.abort();
    // }, 2000);

    /**
     * Observable based Request
     */
    // function ajax<T>(url): Observable<T> {
    //   // TODO
    // }
    // const sub = ajax(apiUrl).subscribe(
    //   data => log('SUCCESS', data),
    //   err => log('ERROR', err)
    // );

    // setTimeout(() => {
    //   // sub.unsubscribe();
    // }, 2000);
  }
}

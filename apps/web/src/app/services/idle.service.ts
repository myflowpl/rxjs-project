import { Injectable } from '@angular/core';
import { Subject, timer, Observable, BehaviorSubject, merge, of } from 'rxjs';
import { switchMap, map, startWith, takeWhile, shareReplay, tap } from 'rxjs/operators';

/**
    Example:

    <span *ngIf="idleService.countdown">
      <b>Session expires in: {{idleService.countdown}}s</b>
      <button (click)="idleService.tick()">Refresh</button>
    </span>
    <span *ngIf="!idleService.isActive">
      No session <button (click)="idleService.activate()">Start</button>
    </span>
 */
@Injectable({
  providedIn: 'root'
})
export class IdleService {

  private maxTime = 6 * 1000;
  private countdownTime = 3 * 1000;

  private _active$ = new BehaviorSubject<boolean>(false);
  private _countdown$ = new BehaviorSubject<number>(0);
  private _tick$ = new Subject<true>();

  get isActive(): boolean {
    return this._active$.getValue();
  }
  get countdown(): number {
    return this._countdown$.getValue();
  }

  constructor() {
    merge(this._active$, this._tick$).pipe(
      switchMap((active) => {
        if (!active) {
          return of(0);
        }
        return timer(this.maxTime - this.countdownTime, 1000).pipe(
          map(n => ((this.countdownTime / 1000) - n)),
          takeWhile(v => v >= 0),
          tap(v => {
            if (!v) {
              this._active$.next(false);
            }
          }),
          startWith(0)
        );
      }),
    ).subscribe(time => this._countdown$.next(time));
  }

  tick() {
    this._tick$.next(true);
  }
  activate() {
    this._active$.next(true);
  }
  deactivate() {
    this._active$.next(false);
  }
}

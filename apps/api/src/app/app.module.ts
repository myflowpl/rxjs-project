import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RevealGateway } from './reveal.gateway';
import { SlidesGateway } from './slides.gateway';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, SlidesGateway]
})
export class AppModule { }

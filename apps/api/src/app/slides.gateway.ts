import { WebSocketGateway, SubscribeMessage, WsResponse, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Client, Server } from 'socket.io';

export interface UserSession {
  data: any;
  client: Client;
}

@WebSocketGateway()
export class SlidesGateway implements OnGatewayConnection, OnGatewayDisconnect {

  users: UserSession[] = [];
  @WebSocketServer()
  server: Server;

  handleConnection(client: Client, ...args: any[]) {
    this.users.push({
      data: args,
      client
    });
    console.log('CONNECT', this.users.length);

  }
  handleDisconnect(client: Client) {
    this.users = this.users.filter(session => session.client !== client);
    console.log('DISCONNECT', this.users.length);

  }

  @SubscribeMessage('change')
  handleEvent(client: Client, data: any) {
    console.log('EVENT', data);
    this.server.emit('update', data);
    // return {
    //   event: 'update',
    //   data
    // };
  }
}
